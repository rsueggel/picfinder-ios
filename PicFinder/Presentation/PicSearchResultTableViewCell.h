//
//  PicSearchResultTableViewCell.h
//  PicFinder
//
//  Created by Robert Süggel on 03.04.17.
//  Copyright © 2017 Fiducia & GAD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PicSearchResultTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *picImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagsLabel;

@end
