//
//  SearchPicViewModel.h
//  PicFinder
//
//  Created by Robert Süggel on 03.04.17.
//  Copyright © 2017 Fiducia & GAD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PicSearchResultTableViewCellModel : NSObject

@property (nonatomic, strong, readonly) NSURL *imageURL;
@property (nonatomic, strong, readonly) NSString *userName;
@property (nonatomic, strong, readonly) NSArray *tagNames;


- (instancetype)initWithImageURL:(NSURL *)imageURL andUserName:(NSString *)userName andTags:(NSArray *)tags;

@end
