//
//  PicSearchResultViewModelMapper.h
//  PicFinder
//
//  Created by Robert Süggel on 03.04.17.
//  Copyright © 2017 Fiducia & GAD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PicSearchResultTableViewCellModel.h"

@interface PicSearchResultViewModelMapper : NSObject

- (NSArray *)picSearchResultTableViewModelsFromBusinessModels:(NSArray *)models;
- (PicSearchResultTableViewCellModel *)picSearchResultTableViewModelFromBusinessModel:(id)model;

@end
