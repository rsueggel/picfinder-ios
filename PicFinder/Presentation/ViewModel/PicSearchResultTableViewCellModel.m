//
//  SearchPicViewModel.m
//  PicFinder
//
//  Created by Robert Süggel on 03.04.17.
//  Copyright © 2017 Fiducia & GAD. All rights reserved.
//

#import "PicSearchResultTableViewCellModel.h"

@implementation PicSearchResultTableViewCellModel

- (instancetype)initWithImageURL:(NSURL *)imageURL
                     andUserName:(NSString *)userName
                         andTags:(NSArray *)tags
{
    self = [super init];
    
    if (self)
    {
        _imageURL = imageURL;
        _userName = userName;
        _tagNames = tags;
    }
    
    return self;
}

@end
