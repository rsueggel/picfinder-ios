//
//  PicSearchResultViewModelMapper.m
//  PicFinder
//
//  Created by Robert Süggel on 03.04.17.
//  Copyright © 2017 Fiducia & GAD. All rights reserved.
//

#import "PicSearchResultViewModelMapper.h"

@implementation PicSearchResultViewModelMapper


- (NSArray *)picSearchResultTableViewModelsFromBusinessModels:(NSArray *)models {
    NSMutableArray *viewModelArray = [[NSMutableArray alloc] initWithCapacity:models.count];
    for (id model in models)
    {
        PicSearchResultTableViewCellModel *mappedObject = [self picSearchResultTableViewModelFromBusinessModel:model];
        if(mappedObject) {
            [viewModelArray addObject:mappedObject];
        }
    }
    return viewModelArray;
}

- (PicSearchResultTableViewCellModel *)picSearchResultTableViewModelFromBusinessModel:(id)model
{
    NSDictionary *dict = model;
    NSURL *imageUrl = [NSURL URLWithString:dict[@"imageUrl"]];
    NSArray *tags = [dict[@"tags"] componentsSeparatedByString:@" "];
    
    PicSearchResultTableViewCellModel *viewModel = [[PicSearchResultTableViewCellModel alloc] initWithImageURL:imageUrl
                                                                                                   andUserName:dict[@"userName"]
                                                                                                       andTags:tags];
    
    return viewModel;
}

@end
