//
//  PicSearchResultTableViewCell.m
//  PicFinder
//
//  Created by Robert Süggel on 03.04.17.
//  Copyright © 2017 Fiducia & GAD. All rights reserved.
//

#import "PicSearchResultTableViewCell.h"

@interface PicSearchResultTableViewCell ()

@end

@implementation PicSearchResultTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
