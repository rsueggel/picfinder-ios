//
//  ImageSearchService.h
//  PicFinder
//
//  Created by Robert Süggel on 03.04.17.
//  Copyright © 2017 Fiducia & GAD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageSearchService.h"

@interface PixaImageSearchService : NSObject <ImageSearchService>

@end
