//
//  ImageSearchService.h
//  PicFinder
//
//  Created by Robert Süggel on 03.04.17.
//  Copyright © 2017 Fiducia & GAD. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ImageSearchService <NSObject>

@required
- (void)searchResultsWithSearchString:(NSString *)searchString andCompletionBlock:(void (^_Nullable)(NSArray *results, NSError *error))completionBlock;

@end
