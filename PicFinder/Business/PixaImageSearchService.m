//
//  ImageSearchService.m
//  PicFinder
//
//  Created by Robert Süggel on 03.04.17.
//  Copyright © 2017 Fiducia & GAD. All rights reserved.
//

#import "PixaImageSearchService.h"

@implementation PixaImageSearchService

- (void)searchResultsWithSearchString:(NSString *)searchString andCompletionBlock:(void (^)(NSArray *, NSError *))completionBlock
{
    // http://fddb.info/static/db/302/LLXQIKUZ5QB8J84FBCCMYCO0_999x999.jpg
    NSArray *apfelResults = @[
                         @{@"imageUrl":@"https://fddb.info/static/db/302/LLXQIKUZ5QB8J84FBCCMYCO0_999x999.jpg", @"userName":@"Wolfgang1", @"tags":@"ios android"},
                         @{@"imageUrl":@"https://www.tedi-shop.com/media/catalog/product/cache/1/image/1000x1000/9df78eab33525d08d6e5fb8d27136e95/d/e/deko-apfel_rosa_201620410610000000_1.jpg", @"userName":@"Robert1", @"tags":@"haha lala"}
                         ];
    
    NSArray *androidResults = @[
                         @{@"imageUrl":@"https://watertrack.de/wp-content/uploads/2015/02/ANDROID.png", @"userName":@"Androidmann", @"tags":@"ios android"},
                         @{@"imageUrl":@"https://www.extremetech.com/wp-content/uploads/2015/01/Android-Broken-348x196.jpg", @"userName":@"Kaputt", @"tags":@"arm ab"}
                         ];
    
    if(completionBlock) {
        // Echten Service aufrufen
        completionBlock(([searchString isEqualToString:@"Apfel"] ? apfelResults : androidResults), nil);
        
    }
}
@end
