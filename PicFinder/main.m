//
//  main.m
//  PicFinder
//
//  Created by Robert Süggel on 03.04.17.
//  Copyright © 2017 Fiducia & GAD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
