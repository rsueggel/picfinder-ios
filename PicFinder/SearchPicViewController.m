//
//  ViewController.m
//  PicFinder
//
//  Created by Robert Süggel on 03.04.17.
//  Copyright © 2017 Fiducia & GAD. All rights reserved.
//

#import "SearchPicViewController.h"
#import "ImageSearchService.h"
#import "PixaImageSearchService.h"

#import "PicSearchResultTableViewCell.h"
#import "PicSearchResultViewModelMapper.h"

@interface SearchPicViewController () <UITableViewDataSource>

@property (nonatomic, strong) NSArray *results;
@property (nonatomic, strong) PicSearchResultViewModelMapper *mapper;

// Services
@property (nonatomic, strong) id<ImageSearchService> searchService;

// Outlets
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;


@end

@implementation SearchPicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.searchService = [PixaImageSearchService new];
}

- (PicSearchResultViewModelMapper *)mapper
{
    if (!_mapper)
    {
        _mapper = [[PicSearchResultViewModelMapper alloc] init];
    }
    return _mapper;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.searchTextField.text = @"Apfel";
    [self search];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)searchButtonTapped:(id)sender {
    NSLog(@"%@", self.searchTextField.text);
    
    [self search];
}

- (void)search {
    [self.searchService searchResultsWithSearchString:self.searchTextField.text
                                   andCompletionBlock:^(NSArray *results, NSError *error) {
                                       
                                       self.results = [self.mapper picSearchResultTableViewModelsFromBusinessModels:results];
                                       
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           [self.tableView reloadData];
                                       });
                                   }];
}

#pragma mark - <TablewView Datasource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.results.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PicSearchResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"resultcell" forIndexPath:indexPath];
    
    PicSearchResultTableViewCellModel *viewModel = self.results[indexPath.row];
    
    if(viewModel.imageURL) {
        NSData *imageData = [NSData dataWithContentsOfURL:viewModel.imageURL];
        cell.picImageView.image = [UIImage imageWithData:imageData];
    }
    cell.userNameLabel.text = viewModel.userName;
    cell.tagsLabel.text = [viewModel.tagNames componentsJoinedByString:@", "];
    
    return cell;
}

@end
